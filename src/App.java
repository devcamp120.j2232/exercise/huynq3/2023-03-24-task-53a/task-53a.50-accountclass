import com.j2232.Account;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Account account1= new Account("18+", "Huy");
        Account account2= new Account("children", "Vũ Thị Yến", 1000);

        System.out.println(account1);
        System.out.println(account2);
        System.out.println("-----------------xem phan dưới là  sub 4------------");
        account1.credit(2000);
        account2.credit(3000);

        System.out.println(account1);
        System.out.println(account2);
        System.out.println("-----------------xem phan dưới là  sub 5------------");

        account1.debit(1000);
        System.out.println(account1);
        
        account2.debit(5000);
        System.out.println(account2);

        System.out.println("-----------------xem phan dưới là  sub 6------------");
        account1.transferTo(account2, 2000);
        System.out.println(account1);
        System.out.println(account2);

        System.out.println("-----------------xem phan dưới là  sub 7-chuyen 2k từ acc2 qua acc1-----------");
        account2.transferTo(account1, 2000);
        System.out.println(account1);

        System.out.println(account2);
    }
}
