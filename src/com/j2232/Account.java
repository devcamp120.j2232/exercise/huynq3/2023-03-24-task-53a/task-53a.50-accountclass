package com.j2232;

public class Account {
    private String id;
    private String name;
    private int balance=0;
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getBalance() {
        return balance;
    }
    public void setBalance(int balance) {
        this.balance = balance;
    }
    public int credit(int amountAdd){
        balance= balance+amountAdd;
        return balance;
    }
    public  int debit(int amountRemove){
        if (amountRemove <= balance){
            balance= balance-amountRemove;
            return balance;
        }
        else {
           
            System.out.println("Số tiền trừ vượt quá khả năng nên giữ nguyên tài khoản");
            return balance;
        }
        
    }
    public int transferTo (Account target, int amount){
       // return source.balance-amount;
       if (amount<=this.balance){
        target.balance=target.balance+amount;
        this.balance=  this.debit(amount);
        return  target.balance ;
        
       }
        else {
            System.out.println("không đủ để chuyển, giữ nguyên");
            return target.balance;
        }
    }
    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }
}
